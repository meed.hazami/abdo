import './App.css';
import Comp from "./comp"

function App() {
  return (
    <div className="App">
      <header className="App-header">
           <Comp></Comp>
      </header>
    </div>
  );
}

export default App;

import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import Input from "@material-ui/core/Input";
import Verification from "./verification";

export default class Comp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      array: [],
      formValidation: { pigeonValidation: "", age: "" },
      loading: false,
    };
  }

  handlerClickYears = () => {
  if (this.state.formValidation.age === "2013") {
      fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/list.json?limit=100&season=150`
      )
        .then((response) => response.json())
        .then((e) =>
          this.setState({
            array: [...this.state.array, ...e].filter(
              (el) => el.racePointName.slice(0, 9) === "Belvedere" ||  el.racePointName.slice(0, 8) === "Pozzallo"
            ),
          })
        );
  }
    else if (this.state.formValidation.age === "2014") {
      fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/list.json?limit=100&season=200`
      )
        .then((response) => response.json())
        .then((e) =>
          this.setState({
            array: [...this.state.array, ...e].filter(
              (el) => el.racePointName.slice(0, 9) === "Belvedere" ||  el.racePointName.slice(0, 8) === "Pozzallo"
            ),
          })
        );
    }
    else if (this.state.formValidation.age === "2016") {
      fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/list.json?limit=100&season=300`
      )
        .then((response) => response.json())
        .then((e) =>
          this.setState({
            array: [...this.state.array, ...e].filter(
              (el) => el.racePointName.slice(0, 9) === "Belvedere" ||  el.racePointName.slice(0, 8) === "Pozzallo"
            ),
          })
        );
    }
   else if (this.state.formValidation.age === "2015") {
      fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/list.json?limit=100&season=250`
      )
        .then((response) => response.json())
        .then((e) =>
          this.setState({
            array: [...this.state.array, ...e].filter(
              (el) => el.racePointName.slice(0, 9) === "Belvedere" ||  el.racePointName.slice(0, 8) === "Pozzallo"
            ),
          })
        );
    } else if (this.state.formValidation.age === "2017") {
      fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/list.json?limit=100&season=350`
      )
        .then((response) => response.json())
        .then((e) =>
          this.setState({
            array: [...this.state.array, ...e].filter(
              (el) => el.racePointName.slice(0, 9) === "Belvedere" ||  el.racePointName.slice(0, 8) === "Pozzallo"
            ),
          })
        );
    } else if (this.state.formValidation.age === "2018") {
      fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/list.json?limit=100&season=400`
      )
        .then((response) => response.json())
        .then((e) =>
          this.setState({
            array: [...this.state.array, ...e].filter(
              (el) => el.racePointName.slice(0, 9) === "Belvedere" ||  el.racePointName.slice(0, 8) === "Pozzallo"
            ),
          })
        );
    } else if (this.state.formValidation.age === "2019") {
      fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/list.json?limit=100&season=450`
      )
        .then((response) => response.json())
        .then((e) =>
          this.setState({
            array: [...this.state.array, ...e].filter(
              (el) => el.racePointName.slice(0, 9) === "Belvedere" ||  el.racePointName.slice(0, 8) === "Pozzallo"
            ),
          })
        );
    } else if (this.state.formValidation.age === "2020") {
      fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/list.json?limit=100&season=500`
      )
        .then((response) => response.json())
        .then((e) =>
          this.setState({
            array: [...this.state.array, ...e].filter(
              (el) => el.racePointName.slice(0, 9) === "Belvedere" ||  el.racePointName.slice(0, 8) === "Pozzallo"
            ),
          })
        );
    } else if (this.state.formValidation.age === "2021") {
      fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/list.json?limit=100&season=550`
      )
        .then((response) => response.json())
        .then((e) =>
          this.setState({
            array: [...this.state.array, ...e].filter(
              (el) => el.racePointName.slice(0, 9) === "Belvedere" ||  el.racePointName.slice(0, 8) === "Pozzallo"
            ),
          })
        );
    } else if (this.state.formValidation.age === "2022") {
      fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/list.json?limit=100&season=600`
      )
        .then((response) => response.json())
        .then((e) =>
          this.setState({
            array: [...this.state.array, ...e].filter(
              (el) => el.racePointName.slice(0, 9) === "Belvedere" ||  el.racePointName.slice(0, 8) === "Pozzallo"
            ),
          })
        );
    } else if (this.state.formValidation.age === "2023") {
      fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/list.json?limit=100&season=650`
      )
        .then((response) => response.json())
        .then((e) =>
          this.setState({
            array: [...this.state.array, ...e].filter(
              (el) => el.racePointName.slice(0, 9) === "Belvedere" ||  el.racePointName.slice(0, 8) === "Pozzallo"
            ),
          })
        );
    } else if (this.state.formValidation.age === "2024") {
      fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/list.json?limit=100&season=700`
      )
        .then((response) => response.json())
        .then((e) =>
          this.setState({
            array: [...this.state.array, ...e].filter(
              (el) => el.racePointName.slice(0, 9) === "Belvedere" ||  el.racePointName.slice(0, 8) === "Pozzallo"
            ),
          })
        );
    } else {

    }
  };

  handlerClick = () => {
    let i = 1;
    let j = 0;
    for (i; i <= this.state.array.length; i++) {
      const response = fetch(
        `http://www.maltafederationofracingpigeons.com:8080/unprot/races/related/${
          this.state.array.length !== 0 && this.state.array[j].id
        }/registers.json?limit=999999999&offset=0`
      )
        .then((response) => response.json())
        .then((e) => this.setState({ data: [...this.state.data, ...e] }));
      this.setState({ loading: true });
      j++;
    }
  };

  handleChange = (prop) => (event) => {
    this.setState({
      formValidation: {
        ...this.state.formValidation,
        [prop]: event.target.value,
      },
    });
  };

  componentDidMount() {
    if (this.state.data.length !== 0) {
      this.setState.states({
        open: true,
        vertical: "top",
        horizontal: "center",
      });
    }
  }

  render() {
    return (
      <>
        {console.log("data", this.state.data)}
        {console.log("arraypigeon", this.state.arraypigeon)}
        {console.log("ipigeon", parseInt(this.state.formValidation.ipigeon))}
        {console.log("array", this.state.array)}

        <div
          style={{
            marginRight: "10px",
            height: "50px",
            display: "flex",
            alignContent: "space-between",
            alignItems: "center",
          }}
        >
          <h3 style={{ marginRight: "10px" }}>Pigeon</h3>
          <div style={{ marginRight: "10px" }}>
            <Input
              id="standard-adornment-description"
              value={this.state.formValidation.pigeonValidation}
              onChange={this.handleChange("pigeonValidation")}
              style={{ color: "#FFFFFF" }}
            />
          </div>
        </div>

        <div
          style={{
            marginRight: "10px",
            height: "50px",
            display: "flex",
            alignContent: "space-between",
            alignItems: "center",
          }}
        >
          <h3 style={{ marginRight: "10px" }}>Années</h3>
          <div style={{ marginRight: "10px" }}>
            <Input
              id="standard-adornment-description"
              value={this.state.formValidation.age}
              onChange={this.handleChange("age")}
              style={{ color: "#FFFFFF" }}
            />
          </div>
        </div>

        <Button
          style={{ width: "325px", margin: "10px" }}
          onClick={() => this.handlerClickYears()}
          variant="contained"
          color="primary"
          fullWidth={true}
        >
          Charger années
        </Button>
        {this.state.array.length !== 0 ? (
          <Button
            style={{ width: "325px", margin: "10px" }}
            onClick={() => this.handlerClick()}
            variant="contained"
            color="primary"
            fullWidth={true}
          >
            Trouver Pigeon
          </Button>
        ) : null}

        {this.state.data.length !== 0 ? null : this.state.loading ? (
          <>
            <h3>Loading</h3> <CircularProgress></CircularProgress>
          </>
        ) : null}
        <div>
          {this.state.data.length !== 0
            ? this.state.data
                .filter(
                  (el) =>
                    el.pigeonRing ===
                    `**-***${this.state.formValidation.pigeonValidation.slice(
                      6,
                      8
                    )}-****`
                )
                .map((val, index) => (
                  <span key={index}>
                    <Verification
                      pigeonVerifier={
                        this.state.formValidation.pigeonValidation
                      }
                      verifierid={val.pigeonIdStr}
                      loading={this.state.loading}
                      pigeonIdStr={val.pigeonIdStr}
                      pigeonRing={val.pigeonRing}
                      childrenFiler={this.state.childrenFiler}
                    ></Verification>
                  </span>
                ))
            : null}
        </div>
      </>
    );
  }
}

import React, { Component } from "react";
import Alert from "@material-ui/lab/Alert";
import Button from "@material-ui/core/Button";

export default class Verification extends Component {
  constructor(props) {
    super(props);
    this.state = { data: "" };
  }

componentDidMount(){
    const apiUrl = `http://www.maltafederationofracingpigeons.com:8080/unprot/pigeons/${this.props.verifierid}.json`;
    fetch(apiUrl)
      .then((response) => response.json())
      .then((e) => this.setState({ data: e }));
}



  render() {
    return (
      <>
        {this.state.data === "" ? null : this.state.data.ring !== (this.props && this.props.pigeonVerifier) ? null : (
          <div
            style={{
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
              backgroundColor: "#4FAF50",
              borderRadius: "3px",
            }}
          >
            <div style={{ margin: "5px" }}>
              {this.props && this.props.pigeonRing}
            </div>
            <Button
              style={{ margin: "20px" }}
              variant="contained"
              color="primary"
              href={`http://www.maltafederationofracingpigeons.com/#/${
                this.props && this.props.pigeonIdStr
              }/pigeon`}
              target="_blank"
            >
              enter Info Pigeon
            </Button>
            <Alert style={{ margin: "20px" }} severity="success">
              OUI
            </Alert>
          </div>
        )}
      </>
    );
  }
}
